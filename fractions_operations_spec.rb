require_relative 'fractions_operations'

describe Fraction do
  describe '#convert' do
    context 'when the number is whole' do
      it 'returns that number' do
        expect(described_class.new('3').convert).to eq(3)
      end
    end

    context 'when the number is fraction' do
      it 'returns a Rational object' do
        expect(described_class.new('1/3').convert).to eq(Rational(1, 3))
      end
    end

    context 'when the number is a fraction with a whole base' do
      it 'returns a Rational object' do
        expect(described_class.new('200_14/2').convert).to be_a(Rational)
      end
    end
  end
end

describe FractionalOperation do
  describe '#calculate' do
    it 'returns the result of a 2 rational operation' do
      expect(described_class.calculate('1/2 + 1/2')).to eq(1)
      expect(described_class.calculate('10/3 - 1')).to eq(Rational(7, 3))
      expect(described_class.calculate('2_1/2 * 1/3')).to eq(Rational(5, 6))
      expect(described_class.calculate('1/8 / 1/8')).to eq(1)
    end

    it 'return the result of more than 2 rational operations' do
      expect(described_class.calculate('1/2 + 1/2 + 1/2')).to eq(Rational(3, 2))
      expect(described_class.calculate('-1/2 + 1/2 - 1/2 + 1/2')).to eq(0)
      expect(described_class.calculate('1/2 + 1/2 + 1/2 * 1_2/3 / 2 * 1/250')).to be_a Rational
    end
  end
end

describe ResultFormatter do
  describe '#build' do
    context 'when the result is a whole number' do
      it 'returns that number' do
        expect(described_class.build(100)).to eq(100)
      end
    end

    context 'when the result is an improper fraction' do
      it 'formats to have the whole portion and the fraction' do
        expect(described_class.build(Rational(10, 3))).to eq('3_1/3')
      end
    end

    context 'when the result is a fraction' do
      it 'returns that fraction' do
        expect(described_class.build(Rational(1, 16))).to eq('1/16')
      end
    end
  end
end
