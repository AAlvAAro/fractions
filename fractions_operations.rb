class Fraction
  def initialize(str)
    @str = str
  end

  def convert
    return @str.to_i unless @str.include?('/')

    split_string
    Rational(@numerator, @denominator)
  end

  private

  def split_string
    head, @denominator = @str.split('/')

    @numerator = head.include?('_') ?  calculate_whole_numerator(head) : head
  end

  def calculate_whole_numerator(head)
    whole, numerator = head.split('_')
    whole_numerator = whole.to_i * @denominator.to_i
    whole_numerator + numerator.to_i
  end
end

class FractionalOperation
  def self.calculate(input)
    elements = input.split(' ')
    fractions = elements.select.with_index { |_, i| i.even? }
    operators = elements - fractions

    result = 0
    while fractions.any? do
      result = Fraction.new(fractions.shift).convert.send(
        operators.shift.to_sym,
        Fraction.new(fractions.shift).convert
      ) if result.zero?

      result = result.send(
        operators.shift.to_sym,
        Fraction.new(fractions.shift).convert
      ) if operators.any?
    end

    result
  end
end

class ResultFormatter
  def self.build(result)
    whole = result.numerator / result.denominator
    reminder = result.numerator % result.denominator

    return result.to_s if whole.zero?
    return whole if reminder.zero?
    "#{whole.floor}_#{reminder}/#{result.denominator}"
  end
end

puts 'Write down some fractional operation such as: 1/2 + 1/2'

begin
  result = FractionalOperation.calculate(gets.chomp)
  result = 0 if result == 0/1
  puts ResultFormatter.build(result)
rescue TypeError => e
  puts 'An error ocurred, please check your input is in the correct form'
rescue NoMethodError => e
  puts 'An error ocurred, please use a valid operator such as +, -, /, *'
rescue => e
  puts "Error: #{e.message}"
end
